package ru.t1consulting.nkolesnik.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskGetByProjectIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskGetByProjectIdRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
